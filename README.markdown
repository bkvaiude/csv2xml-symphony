# CSV2XML

* Version: 1.1
* Author: Bhushan Vaiude (https://github.com/bkvaiude)
* Build Date: 2013-07-30
* Requirements: Symphony 2.2

Upload a csv file & get xml output of uploaded xml file into 
a readonly textarea field that only accepts valid XML.