<?php

	if( !defined('__IN_SYMPHONY__') ) die('<h2>Symphony Error</h2><p>You cannot directly access this file</p>');



	require_once(TOOLKIT.'/fields/field.upload.php');




	final class FieldCSV2XML extends fieldUpload
	{

		/*------------------------------------------------------------------------------------------------*/
		/*  Definition  */
		/*------------------------------------------------------------------------------------------------*/

		public function __construct(){
			parent::__construct();			
			$this->_name = 'CSV2XML';		
			$this->_required = true;

			// Set defaults
			$this->set('show_column', 'no');
			$this->set('required', 'yes');
		}

		function checkPostFieldData($data, &$message, $entry_id=NULL){
			$result  = parent::checkPostFieldData($data, $message, $entry_id);
			
			return $result;
		}
		public function processRawFieldData($data, &$status, &$message=null, $simulate = false, $entry_id = null) {
			
			$result  = parent::processRawFieldData($data, $status, $message, $simulate, $entry_id);
			
			$result['value'] = $this->___parseCsv2Xml($this->getFilePath($result['file']));
			return $result;
		}
		public function displaySettingsPanel(&$wrapper, $errors = null) {
			parent::displaySettingsPanel($wrapper, $errors);
		}
		function createTable(){
			return Symphony::Database()->query(
				"CREATE TABLE IF NOT EXISTS `tbl_entries_data_" . $this->get('id') . "` (
				  `id` int(11) unsigned NOT NULL auto_increment,
				  `entry_id` int(11) unsigned NOT NULL,
				  `file` varchar(255) default NULL,
				  `size` int(11) unsigned NULL,
				  `mimetype` varchar(100) default NULL,
				  `meta` varchar(255) default NULL,
				  `value` text,
				  PRIMARY KEY  (`id`),
				  KEY `entry_id` (`entry_id`),
				  FULLTEXT KEY `value` (`value`)
				) TYPE=MyISAM;"
			
			);
		}
		
		//Xml Gen function
		function ___array_to_xml($template_info, &$xml_template_info) {
            foreach($template_info as $key => $value) {
                if(is_array($value)) {
                    if(!is_numeric($key)){
 
                        $subnode = $xml_template_info->addChild("$key");
 
                        if(count($value) >1 && is_array($value)){
                            $jump = false;
                            $count = 1;
                            foreach($value as $k => $v) {
                                if(is_array($v)){
                                    if($count++ > 1)
                                        $subnode = $xml_template_info->addChild("$key");
 
                                    $this->___array_to_xml($v, $subnode);
                                    $jump = true;
                                }
                            }
                            if($jump) {
                                goto LE;
                            }
                            $this->___array_to_xml($value, $subnode);
                        }
                        else
                            $this->___array_to_xml($value, $subnode);
                    }
                    else{
                        $this->___array_to_xml($value, $xml_template_info);
                    }
                }
                else {
                    $xml_template_info->addChild("$key","$value");
                }
 
                LE: ;
            }
        }

		function ___getFormattedDate($eventDate,$hours){
			$dateParts=explode("/", $eventDate);
			$month = intVal($dateParts[0]);
			$day = intVal($dateParts[1]);
			$year = intVal($dateParts[2]);
			$eventFormattedDate = date("Y-m-d",mktime($hours,0,0,$month,$day,$year));
			return $eventFormattedDate;	
		}

		function ___filterDataForRequiredJOSNFormat($fileRow){
			$filteredOutput = array();
			if($fileRow[0] != "House No"){
				$eventDate = $fileRow[2];
				$start_time = $fileRow[3];
				$start_time_parts = str_split($start_time,2);
				@$filteredOutput['start_time'] = date("H:i", mktime(($start_time_parts[0] + $timezone),$start_time_parts[1],0,0,0,0));
				@$filteredOutput['formatted_event_date'] = $this->___getFormattedDate($eventDate,($start_time_parts[0] + $timezone));
				$filteredOutput['episode_title'] = trim($fileRow[4]);
				$filteredOutput['title'] = trim($fileRow[5]);
				$filteredOutput['duration'] = trim($fileRow[6]);
				$filteredOutput['event_episode_number'] = trim($fileRow[7]);
				@$filteredOutput['source'] = trim($fileRow[9]);
				$endTime = intval($start_time)+intval($filteredOutput['duration']);
				$endTimeParts = str_split(str_pad($endTime, 4, "000", STR_PAD_LEFT),2);
				@$endTime = date("H:i", mktime(($endTimeParts[0] + $timezone),$endTimeParts[1],0,0,0,0));
				$filteredOutput['end_time'] = $endTime;
			}
			return $filteredOutput;
		}		

		function ___parseCsv2Xml($filename){
			$container = array();

			$file = fopen($filename,"r");

			while(! feof($file)){
				$fileRow = fgetcsv($file);
				$filteredOutputRow =  $this->___filterDataForRequiredJOSNFormat($fileRow);
				if(!empty($filteredOutputRow)) 
					$container[] = $filteredOutputRow;
			}

			fclose($file);
			$output = array('event'=>$container);

			//xml generater
			// creating object of SimpleXMLElement
			$xml_template_info = new SimpleXMLElement("<events></events>");
			 
			// function call to convert array to xml
			$this->___array_to_xml($output,$xml_template_info);

			//print_r($xml_template_info);
			//saving generated xml file
			 return $xml_template_info->asXML() ;
			//xml generator
		}
	}
